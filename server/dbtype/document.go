package dbtype

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Provider struct {
	ID   string `json:"id" bson:"id"`
	Name string `json:"name" bson:"name"`
	Addr string `json:"addr" bson:"addr"`
	INN  string `json:"inn" bson:"inn"`
}

type Product struct {
	ID    string  `json:"id" bson:"id"`
	Name  string  `json:"name" bson:"name"`
	Price float64 `json:"price" bson:"price"`
	Count float64 `json:"count" bson:"count"`
	Sum   float64 `json:"sum" bson:"sum"`
}

type Document struct {
	ID          primitive.ObjectID     `bson:"_id" json:"id"`
	Owner       primitive.ObjectID     `bson:"owner" json:"-"`
	Type        string                 `bson:"type" json:"type"`
	Confirm     bool                   `bson:"confirm" json:"confirm"`
	Number      string                 `bson:"number" json:"number"`
	DateEdit    time.Time              `bson:"dateEdit" json:"dateEdit"`
	DateCreated time.Time              `bson:"dateCreated" json:"dateCreated"`
	Provider    Provider               `bson:"provider" json:"provider"`
	Items       []Product              `bson:"items" json:"items"`
	ExtFields   map[string]interface{} `bson:"extFields" json:"extFields"`
}
