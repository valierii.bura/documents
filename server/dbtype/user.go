package dbtype

import "go.mongodb.org/mongo-driver/bson/primitive"

type User struct {
	ID       primitive.ObjectID `bson:"_id" json:"id"`
	Login    string             `bson:"login" json:"login"`
	Name     string             `bson:"name" json:"name"`
	Password []byte             `bson:"password" json:"password"`
	Salt     []byte             `bson:"salt" json:"salt"`
	Access   []string           `bson:"access" json:"access"`
}
