package dbtype

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Session struct {
	ID         string             `bson:"_id" json:"id"`
	Access     []string           `bson:"access" json:"access"`
	Link       primitive.ObjectID `bson:"link" json:"link"`
	LastUpdate time.Time          `bson:"lastUpdate" json:"lastUpdate"`
}
