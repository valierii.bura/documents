package users

import (
	"encoding/json"
	"gitlab.com/documents/server/api/jsongears"
	"gitlab.com/documents/server/constants"
	"gitlab.com/documents/server/dbtype"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
)

func (a *Users) Get(w http.ResponseWriter, r *http.Request) {
	ses, err := jsongears.CheckSession(r)
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	session, err := jsongears.GetSession(a.client, ses)
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:     constants.CookieName,
		Value:    session.ID,
		MaxAge:   3600,
		Domain:   r.Host,
		Path:     "/",
		HttpOnly: true,
	})

	var users []dbtype.User
	cu := a.client.Database("documents").Collection("user")

	cur, err := cu.Find(nil, bson.M{})
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}
	err = cur.All(nil, &users)
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	encoder := json.NewEncoder(w)

	_ = encoder.Encode(users)
}
