package users

import (
	"encoding/json"
	"gitlab.com/documents/server/api/jsongears"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
)

type SendToReq struct {
	ID primitive.ObjectID `json:"id"`
	To primitive.ObjectID `json:"to"`
}

func (a *Users) SendTo(w http.ResponseWriter, r *http.Request) {
	ses, err := jsongears.CheckSession(r)
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	_, err = jsongears.GetSession(a.client, ses)
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	var sendToReq SendToReq
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&sendToReq); err != nil {
		jsongears.SendErr(w, err)
		return
	}

	cu := a.client.Database("documents").Collection("document")

	find := bson.M{
		"_id": sendToReq.ID,
	}
	update := bson.M{
		"owner": sendToReq.To,
	}

	_, err = cu.UpdateOne(nil, find, bson.M{"$set": update})
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	encoder := json.NewEncoder(w)

	_ = encoder.Encode(bson.M{"status": true})
}
