package users

import "go.mongodb.org/mongo-driver/mongo"

type Users struct {
	client *mongo.Client
}

func New(client *mongo.Client) *Users {
	return &Users{client: client}
}
