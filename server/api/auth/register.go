package auth

import (
	"crypto/sha256"
	"encoding/json"
	"gitlab.com/documents/server/api/jsongears"
	"gitlab.com/documents/server/constants"
	"gitlab.com/documents/server/dbtype"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"math/rand"
	"net/http"
)

type registerReq struct {
	Login string `json:"login"`
	Password string `json:"password"`
	Access []string `json:"access"`
}

type Status struct {
	Status bool `json:"status"`
}

func (a *Auth) Register(w http.ResponseWriter, r *http.Request) {
	var registerForm registerReq
	dec := json.NewDecoder(r.Body)
	if err := dec.Decode(&registerForm); err != nil {
		jsongears.SendErr(w, ErrInvalidType)
		return
	}

	if registerForm.Login == "" {
		jsongears.SendErr(w, ErrLoginIsEmpty)
		return
	}

	if registerForm.Password == "" {
		jsongears.SendErr(w, ErrPasswordIsEmpty)
		return
	}

	cu := a.client.Database("documents").Collection("user")

	salt := make([]byte, 32)
	if _, err := rand.Read(salt); err != nil {
		jsongears.SendErr(w, ErrIncorrectLoginOrPassword)
		return
	}

	password := sha256.New()
	password.Write([]byte(registerForm.Password))
	password.Write(salt)

	if len(registerForm.Access) == 0 {
		registerForm.Access = []string{constants.AccessDefault}
	}

	_, err := cu.InsertOne(nil, dbtype.User{
		ID:       primitive.NewObjectID(),
		Login:    registerForm.Login,
		Password: password.Sum(nil),
		Salt:     salt,
		Access:   registerForm.Access,
	})
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	encoder := json.NewEncoder(w)
	_ = encoder.Encode(Status{Status: true})
}
