package auth

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"gitlab.com/documents/server/api/jsongears"
	"gitlab.com/documents/server/constants"
	"gitlab.com/documents/server/dbtype"
	"go.mongodb.org/mongo-driver/bson"
	"math/rand"
	"net/http"
	"time"
)

var (
	ErrInvalidType              = errors.New("invalid login type")
	ErrLoginIsEmpty             = errors.New("login is empty")
	ErrPasswordIsEmpty          = errors.New("password is empty")
	ErrIncorrectLoginOrPassword = errors.New("invalid login or password")
)

type AuthForm struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type AuthResponse struct {
	Session string   `json:"session"`
	Access  []string `json:"access"`
}

func (a *Auth) Login(w http.ResponseWriter, r *http.Request) {

	var authForm AuthForm
	dec := json.NewDecoder(r.Body)
	if err := dec.Decode(&authForm); err != nil {
		jsongears.SendErr(w, ErrInvalidType)
		return
	}

	if authForm.Login == "" {
		jsongears.SendErr(w, ErrLoginIsEmpty)
		return
	}

	if authForm.Password == "" {
		jsongears.SendErr(w, ErrPasswordIsEmpty)
		return
	}

	ca := a.client.Database("documents").Collection("session")

	cu := a.client.Database("documents").Collection("user")
	var user dbtype.User
	if err := cu.FindOne(nil, bson.M{
		"login": authForm.Login,
	}).Decode(&user); err != nil {
		jsongears.SendErr(w, err)
		return
	}

	sha := sha256.New()
	sha.Write([]byte(authForm.Password))
	sha.Write(user.Salt)

	if !bytes.Equal(sha.Sum(nil), user.Password) {
		jsongears.SendErr(w, ErrIncorrectLoginOrPassword)
		return
	}

	source := rand.NewSource(time.Now().Unix())
	rrand := rand.New(source)

	rnd := make([]byte, 32)
	if _, err := rrand.Read(rnd); err != nil {
		jsongears.SendErr(w, err)
		return
	}

	rndHex := hex.EncodeToString(rnd)

	if _, err := ca.InsertOne(nil, &dbtype.Session{
		ID:         rndHex,
		Link:       user.ID,
		Access:     user.Access,
		LastUpdate: time.Now(),
	}); err != nil {
		jsongears.SendErr(w, err)
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:     constants.CookieName,
		Value:    rndHex,
		MaxAge:   3600,
		Domain:   r.Host,
		Path:     "/",
		HttpOnly: true,
		SameSite: http.SameSiteNoneMode,
		Secure:   true,
	})

	encoder := json.NewEncoder(w)

	resp := AuthResponse{
		Session: rndHex,
		Access:  user.Access,
	}

	_ = encoder.Encode(resp)
}
