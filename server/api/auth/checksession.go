package auth

import (
	"encoding/json"
	"gitlab.com/documents/server/api/jsongears"
	"gitlab.com/documents/server/constants"
	"net/http"
)

type SessReq struct {
	Session string `json:"session"`
}

func (a *Auth) CheckSession(w http.ResponseWriter, r *http.Request) {
	ses, err := jsongears.CheckSession(r)
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	session, err := jsongears.GetSession(a.client, ses)
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:     constants.CookieName,
		Value:    session.ID,
		MaxAge:   3600,
		Domain:   r.Host,
		Path:     "/",
		HttpOnly: true,
	})

	encoder := json.NewEncoder(w)

	resp := AuthResponse{
		Session: session.ID,
		Access:  session.Access,
	}

	_ = encoder.Encode(resp)
}
