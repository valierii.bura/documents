package auth

import "go.mongodb.org/mongo-driver/mongo"

type Auth struct {
	client *mongo.Client
}

func New(client *mongo.Client) *Auth {
	return &Auth{client: client}
}


