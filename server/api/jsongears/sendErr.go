package jsongears

import (
	"encoding/json"
	"net/http"
)

type Err struct {
	Error string `json:"error"`
}

func SendErr(w http.ResponseWriter, err error) {
	if err == nil {
		return
	}

	w.WriteHeader(http.StatusBadRequest)
	errStruct := Err{
		Error: err.Error(),
	}

	encoder := json.NewEncoder(w)

	_ = encoder.Encode(errStruct)
}
