package jsongears

import (
	"errors"
	"github.com/globalsign/mgo/bson"
	"gitlab.com/documents/server/dbtype"
	"go.mongodb.org/mongo-driver/mongo"
)

func GetSession(client *mongo.Client, session string) (*dbtype.Session, error) {
	if session == "" {
		return nil, errors.New("empty session")
	}

	cs := client.Database("documents").Collection("session")

	var ses dbtype.Session
	err := cs.FindOne(nil, bson.M{"_id": session}).Decode(&ses)
	if err != nil {
		return nil, err
	}

	return &ses, nil
}
