package jsongears

import (
	"errors"
	"gitlab.com/documents/server/constants"
	"net/http"
)

func CheckSession(r *http.Request) (string, error) {
	cookie, err := r.Cookie(constants.CookieName)
	if err == http.ErrNoCookie || cookie == nil {
		return "", errors.New("session not found")
	}

	return cookie.Value, nil
}
