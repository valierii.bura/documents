package products

import (
	"encoding/json"
	"errors"
	"gitlab.com/documents/server/api/jsongears"
	"gitlab.com/documents/server/dbtype"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
)

var (
	ErrEmptySession = errors.New("empty session")
)

func (p *Product) List(w http.ResponseWriter, r *http.Request) {
	sesssion, err := jsongears.CheckSession(r)
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	_, err = jsongears.GetSession(p.client, sesssion)
	if err != nil {
		jsongears.SendErr(w, ErrEmptySession)
		return
	}

	cp := p.client.Database("documents").Collection("product")

	find := bson.M{}

	products := make([]dbtype.Product, 0)
	cur, err := cp.Find(nil, find)
	if err != nil {
		jsongears.SendErr(w, errors.New("products not found"))
		return
	}

	if err = cur.All(nil, &products); err != nil {
		jsongears.SendErr(w, errors.New("unmarshall error"))
		return
	}

	encoder := json.NewEncoder(w)
	_ = encoder.Encode(products)
}
