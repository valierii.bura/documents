package products

import (
	"encoding/json"
	"errors"
	"gitlab.com/documents/server/api/jsongears"
	"gitlab.com/documents/server/constants"
	"gitlab.com/documents/server/dbtype"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
)

func (p *Product) Edit(w http.ResponseWriter, r *http.Request) {
	sesssion, err := jsongears.CheckSession(r)
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	ses, err := jsongears.GetSession(p.client, sesssion)
	if err != nil {
		jsongears.SendErr(w, ErrEmptySession)
		return
	}

	isAdmin := false
	for _, access := range ses.Access {
		if access == constants.AccessAdmin {
			isAdmin = true
		}
	}

	if !isAdmin {
		jsongears.SendErr(w, errors.New("only admin have permissions for editing new providers"))
		return
	}

	var product dbtype.Product
	dec := json.NewDecoder(r.Body)
	if err = dec.Decode(&product); err != nil {
		jsongears.SendErr(w, err)
		return
	}

	cd := p.client.Database("documents").Collection("product")

	find := bson.M{
		"_id": product.ID,
	}

	update := bson.M{
		"$set": bson.M{
			"name":  product.Name,
			"price": product.Price,
			"count": product.Count,
			"sum":   product.Sum,
		},
	}

	_, err = cd.UpdateOne(nil, find, update)
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	encoder := json.NewEncoder(w)
	_ = encoder.Encode(bson.M{"status": true})
}
