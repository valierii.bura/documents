package products

import "go.mongodb.org/mongo-driver/mongo"

type Product struct {
	client *mongo.Client
}

func New(client *mongo.Client) *Product {
	return &Product{client: client}
}
