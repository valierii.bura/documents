package documents

import (
	"encoding/json"
	"errors"
	"gitlab.com/documents/server/api/jsongears"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
)

type ConfirmReq struct {
	ID primitive.ObjectID
}

func (d *Document) Confirm(w http.ResponseWriter, r *http.Request) {
	sesssion, err := jsongears.CheckSession(r)
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	_, err = jsongears.GetSession(d.client, sesssion)
	if err != nil {
		jsongears.SendErr(w, ErrEmptySession)
		return
	}

	var confirmReq ConfirmReq
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&confirmReq); err != nil {
		jsongears.SendErr(w, errors.New("invalid document type"))
		return
	}

	cd := d.client.Database("documents").Collection("document")
	find := bson.M{
		"_id": confirmReq.ID,
	}

	set := bson.M{
		"confirm": true,
	}

	update := bson.M{
		"$set": set,
	}

	_, err = cd.UpdateOne(nil, find, update)
	if err != nil {
		jsongears.SendErr(w, errors.New("error document insert"))
		return
	}

	encoder := json.NewEncoder(w)
	_ = encoder.Encode(Status{Status: true})
}
