package documents

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/documents/server/api/jsongears"
	"gitlab.com/documents/server/constants"
	"gitlab.com/documents/server/dbtype"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"time"
)

type StatusID struct {
	ID     string `json:"id"`
	Status bool   `json:"status"`
}

func (d *Document) Add(w http.ResponseWriter, r *http.Request) {
	sesssion, err := jsongears.CheckSession(r)
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	ses, err := jsongears.GetSession(d.client, sesssion)
	if err != nil {
		jsongears.SendErr(w, ErrEmptySession)
		return
	}
	fmt.Println(1)

	var document dbtype.Document
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&document); err != nil {
		fmt.Println(err)
		//jsongears.SendErr(w, errors.New("invalid document type"))
		jsongears.SendErr(w, err)
		return
	}

	if document.Type == "" {
		jsongears.SendErr(w, errors.New("invalid type of document"))
		return
	}

	if document.Number == "" {
		jsongears.SendErr(w, errors.New("invalid number of document"))
		return
	}

	if d.checkDocuments(w, document) {
		return
	}

	cd := d.client.Database("documents").Collection("document")

	isAdmin := false
	for _, access := range ses.Access {
		if access == constants.AccessAdmin {
			isAdmin = true
			break
		}
	}

	if !isAdmin {
		document.Owner = ses.Link
	}
	document.ID = primitive.NewObjectID()
	document.DateCreated = time.Now()
	document.DateEdit = time.Now()

	_, err = cd.InsertOne(nil, document)
	if err != nil {
		jsongears.SendErr(w, errors.New("error document insert"))
		return
	}

	encoder := json.NewEncoder(w)
	_ = encoder.Encode(StatusID{Status: true, ID: document.ID.Hex()})
}
