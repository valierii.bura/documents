package documents

import "go.mongodb.org/mongo-driver/mongo"

type Document struct {
	client *mongo.Client
}

func New(client *mongo.Client) *Document {
	return &Document{client: client}
}


