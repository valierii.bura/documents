package documents

import (
	"encoding/json"
	"errors"
	"gitlab.com/documents/server/api/jsongears"
	"gitlab.com/documents/server/constants"
	"gitlab.com/documents/server/dbtype"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
)

func (d *Document) ByID(w http.ResponseWriter, r *http.Request) {
	sesssion, err := jsongears.CheckSession(r)
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	ses, err := jsongears.GetSession(d.client, sesssion)
	if err != nil {
		jsongears.SendErr(w, ErrEmptySession)
		return
	}

	docIDStr := r.FormValue("id")
	docID, err := primitive.ObjectIDFromHex(docIDStr)
	if err != nil {
		jsongears.SendErr(w, errors.New("invalid document id"))
		return
	}

	cd := d.client.Database("documents").Collection("document")


	isAdmin := false
	for _, access := range ses.Access {
		if access == constants.AccessAdmin {
			isAdmin = true
			break
		}
	}

	find := bson.M{
		"_id": docID,
	}

	if !isAdmin {
		find["owner"] = ses.Link
	}

	owner := r.FormValue("owner")
	if isAdmin && owner != "" {
		id, err := primitive.ObjectIDFromHex(owner)
		if err != nil {
			jsongears.SendErr(w, errors.New("invalid owner value"))
			return
		}

		find["owner"] = id
	}

	var document dbtype.Document
	err = cd.FindOne(nil, find).Decode(&document)
	if err != nil {
		jsongears.SendErr(w, errors.New("document not found"))
		return
	}

	encoder := json.NewEncoder(w)
	_ = encoder.Encode(document)
}
