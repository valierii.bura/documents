package documents

import (
	"encoding/json"
	"errors"
	"gitlab.com/documents/server/api/jsongears"
	"gitlab.com/documents/server/constants"
	"gitlab.com/documents/server/dbtype"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
	"time"
)

type Status struct {
	Status bool `json:"status"`
}

func (d *Document) Edit(w http.ResponseWriter, r *http.Request) {
	sesssion, err := jsongears.CheckSession(r)
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	ses, err := jsongears.GetSession(d.client, sesssion)
	if err != nil {
		jsongears.SendErr(w, ErrEmptySession)
		return
	}

	var document dbtype.Document
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&document); err != nil {
		jsongears.SendErr(w, errors.New("invalid document type"))
		return
	}

	cd := d.client.Database("documents").Collection("document")
	find := bson.M{
		"_id": document.ID,
	}

	if d.checkDocuments(w, document) {
		return
	}

	set := bson.M{
		"type":     document.Type,
		"provider": document.Provider,
		"items":    document.Items,
		"dateEdit": time.Now(),
	}

	isAdmin := false
	for _, access := range ses.Access {
		if access == constants.AccessAdmin {
			isAdmin = true
			break
		}
	}

	if !isAdmin {
		find["owner"] = document.Owner
	} else {
		set["owner"] = document.Owner
	}

	update := bson.M{
		"$set": set,
	}

	_, err = cd.UpdateOne(nil, find, update)
	if err != nil {
		jsongears.SendErr(w, errors.New("error document insert"))
		return
	}

	encoder := json.NewEncoder(w)
	_ = encoder.Encode(Status{Status: true})
}

func (d *Document) checkDocuments(w http.ResponseWriter, document dbtype.Document) bool {
	if document.Type == "" {
		jsongears.SendErr(w, errors.New("invalid type of document"))
		return true
	}

	if document.Provider.Name == "" ||
		document.Provider.ID == "" ||
		document.Provider.INN == "" ||
		document.Provider.Addr == "" {
		jsongears.SendErr(w, errors.New("invalid provider"))
		return true
	}

	if len(document.Items) == 0 {
		jsongears.SendErr(w, errors.New("items can't be empty"))
		return true
	}

	for _, item := range document.Items {
		if item.Name == "" {
			jsongears.SendErr(w, errors.New("invalid item name"))
			return true
		}

		if item.Count == 0 {
			jsongears.SendErr(w, errors.New("invalid item count"))
			return true
		}

		if item.ID == "" {
			jsongears.SendErr(w, errors.New("invalid item id"))
			return true
		}
	}
	return false
}
