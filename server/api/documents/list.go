package documents

import (
	"encoding/json"
	"errors"
	"gitlab.com/documents/server/api/jsongears"
	"gitlab.com/documents/server/constants"
	"gitlab.com/documents/server/dbtype"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
)

var (
	ErrEmptySession = errors.New("empty session")
)

func (d *Document) List(w http.ResponseWriter, r *http.Request) {
	sesssion, err := jsongears.CheckSession(r)
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	ses, err := jsongears.GetSession(d.client, sesssion)
	if err != nil {
		jsongears.SendErr(w, ErrEmptySession)
		return
	}

	cd := d.client.Database("documents").Collection("document")

	isAdmin := false
	for _, access := range ses.Access {
		if access == constants.AccessAdmin {
			isAdmin = true
			break
		}
	}

	find := bson.M{}
	if !isAdmin {
		find["owner"] = ses.Link
	}

	owner := r.FormValue("owner")
	if isAdmin && owner != "" {
		id, err := primitive.ObjectIDFromHex(owner)
		if err != nil {
			jsongears.SendErr(w, errors.New("invalid owner value"))
			return
		}

		find["owner"] = id
	}

	documents := make([]dbtype.Document, 0)
	cur, err := cd.Find(nil, find)
	if err != nil {
		jsongears.SendErr(w, errors.New("documents not found"))
		return
	}

	if err = cur.All(nil, &documents); err != nil {
		jsongears.SendErr(w, errors.New("unmarshall error"))
		return
	}

	encoder := json.NewEncoder(w)
	_ = encoder.Encode(documents)
}
