package providers

import (
	"encoding/json"
	"errors"
	"gitlab.com/documents/server/api/jsongears"
	"gitlab.com/documents/server/constants"
	"gitlab.com/documents/server/dbtype"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
)

func (p *Provider) Edit(w http.ResponseWriter, r *http.Request) {
	sesssion, err := jsongears.CheckSession(r)
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	ses, err := jsongears.GetSession(p.client, sesssion)
	if err != nil {
		jsongears.SendErr(w, ErrEmptySession)
		return
	}

	isAdmin := false
	for _, access := range ses.Access {
		if access == constants.AccessAdmin {
			isAdmin = true
		}
	}

	if !isAdmin {
		jsongears.SendErr(w, errors.New("only admin have permissions for editing new providers"))
		return
	}

	var provider dbtype.Provider
	dec := json.NewDecoder(r.Body)
	if err = dec.Decode(&provider); err != nil {
		jsongears.SendErr(w, err)
		return
	}

	cd := p.client.Database("documents").Collection("provider")

	find := bson.M{
		"_id": provider.ID,
	}

	update := bson.M{
		"$set": bson.M{
			"name": provider.Name,
			"addr": provider.Addr,
			"inn":  provider.INN,
		},
	}

	_, err = cd.UpdateOne(nil, find, update)
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	encoder := json.NewEncoder(w)
	_ = encoder.Encode(bson.M{"status": true})
}
