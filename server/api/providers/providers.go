package providers

import "go.mongodb.org/mongo-driver/mongo"

type Provider struct {
	client *mongo.Client
}

func New(client *mongo.Client) *Provider {
	return &Provider{client: client}
}
