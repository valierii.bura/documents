package providers

import (
	"encoding/json"
	"errors"
	"gitlab.com/documents/server/api/jsongears"
	"gitlab.com/documents/server/dbtype"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
)

var (
	ErrEmptySession = errors.New("empty session")
)

func (p *Provider) List(w http.ResponseWriter, r *http.Request) {
	sesssion, err := jsongears.CheckSession(r)
	if err != nil {
		jsongears.SendErr(w, err)
		return
	}

	_, err = jsongears.GetSession(p.client, sesssion)
	if err != nil {
		jsongears.SendErr(w, ErrEmptySession)
		return
	}

	cd := p.client.Database("documents").Collection("provider")

	find := bson.M{}

	providers := make([]dbtype.Provider, 0)
	cur, err := cd.Find(nil, find)
	if err != nil {
		jsongears.SendErr(w, errors.New("documents not found"))
		return
	}

	if err = cur.All(nil, &providers); err != nil {
		jsongears.SendErr(w, errors.New("unmarshall error"))
		return
	}

	encoder := json.NewEncoder(w)
	_ = encoder.Encode(providers)
}
