package debugger

import "fmt"

var DBG *Debugger

type Debugger struct {
	debug bool
}

func New(debug bool) {
	if DBG != nil {
		return
	}

	DBG = &Debugger{debug: debug}
}

func (d *Debugger) Print(values ...interface{})  {
	if !d.debug {
		return
	}

	fmt.Println(values...)
}
