package main

import (
	"context"
	"github.com/nexcode/wenex"
	"gitlab.com/documents/server/api/auth"
	"gitlab.com/documents/server/api/documents"
	"gitlab.com/documents/server/api/jsongears"
	"gitlab.com/documents/server/api/products"
	"gitlab.com/documents/server/api/providers"
	"gitlab.com/documents/server/api/users"
	"gitlab.com/documents/server/debugger"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"net/http"
	"time"
)

func main() {

	// Init Debugger for management debug logs
	debugger.New(true)

	// Connect to MongoDB
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://127.0.0.1:27017"))
	if err != nil {
		panic(err)
	}

	err = client.Ping(nil, readpref.Primary())
	if err != nil {
		panic(err)
	}

	debugger.DBG.Print("Connection established")

	defer func() {
		if err = client.Disconnect(ctx); err != nil {
			panic(err)
		}
	}()

	// Init wenex for web server
	wnx, err := wenex.New("documents", wenex.DefaultConfig(), nil)
	if err != nil {
		panic(err)
	}

	debugger.DBG.Print("Wenex was init. Listen: ", wnx.Config.MustString("server.http.listen"))

	// Auth route
	authObj := auth.New(client)
	wnx.Router.StrictRoute("/auth/login", http.MethodPost).MustChain(jsongears.AddHeader, authObj.Login)
	wnx.Router.StrictRoute("/auth/register", http.MethodPost).MustChain(jsongears.AddHeader, authObj.Register)
	wnx.Router.StrictRoute("/auth/checksession", http.MethodPost).MustChain(jsongears.AddHeader, authObj.CheckSession)
	// Documents route
	docObj := documents.New(client)
	wnx.Router.StrictRoute("/document/list", http.MethodGet).MustChain(jsongears.AddHeader, docObj.List)
	wnx.Router.StrictRoute("/document/:id", http.MethodGet).MustChain(jsongears.AddHeader, docObj.ByID)
	//wnx.Router.StrictRoute("/document/:id", http.MethodPost).MustChain(jsongears.AddHeader, docObj.Edit)
	wnx.Router.StrictRoute("/document/add", http.MethodPost).MustChain(jsongears.AddHeader, docObj.Add)
	wnx.Router.StrictRoute("/document/confirm", http.MethodPost).MustChain(jsongears.AddHeader, docObj.Confirm)

	// Users route
	userObj := users.New(client)
	wnx.Router.StrictRoute("/users/all", http.MethodGet).MustChain(jsongears.AddHeader, userObj.Get)
	wnx.Router.StrictRoute("/users/send-to", http.MethodPost).MustChain(jsongears.AddHeader, userObj.SendTo)

	// Providers route
	providerObj := providers.New(client)
	wnx.Router.StrictRoute("/provider/all", http.MethodGet).MustChain(jsongears.AddHeader, providerObj.List)
	wnx.Router.StrictRoute("/provider/add", http.MethodPost).MustChain(jsongears.AddHeader, providerObj.Add)
	wnx.Router.StrictRoute("/provider/edit", http.MethodPost).MustChain(jsongears.AddHeader, providerObj.Edit)

	// Products route
	productObj := products.New(client)
	wnx.Router.StrictRoute("/product/all", http.MethodGet).MustChain(jsongears.AddHeader, productObj.List)
	wnx.Router.StrictRoute("/product/add", http.MethodPost).MustChain(jsongears.AddHeader, productObj.Add)
	wnx.Router.StrictRoute("/product/edit", http.MethodPost).MustChain(jsongears.AddHeader, productObj.Edit)

	if err = wnx.Run(); err != nil {
		panic(err)
	}
}
