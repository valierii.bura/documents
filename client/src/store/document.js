
const document = {
    state: () => ({
        documents: [],
        users: [],
    }),
    mutations: {
        setDocuments (state, documents) {
            state.documents = documents
        },
        setUsers (state, users) {
            state.users = users
        }
    },
    actions: {
        getList({commit, dispatch}, data) {
            return new Promise((resolve, reject) => {
                dispatch('get', {
                    name: 'document/list', data: data
                }).then(
                    json => {
                        commit('setDocuments', json)
                        resolve();
                    },
                    err => reject(err)
                );
            });
        },
        add({dispatch}, doc) {
            return new Promise((resolve, reject) => {
                dispatch('post', {
                    name: 'document/add', data: doc
                }).then(
                    () => {
                        dispatch('getList')
                        resolve();
                    },
                    err => reject(err)
                );
            });
        },
        sendTo({dispatch}, doc) {
            return new Promise((resolve, reject) => {
                dispatch('post', {
                    name: 'users/send-to', data: doc
                }).then(
                    () => {
                        dispatch('getList')
                        resolve();
                    },
                    err => reject(err)
                );
            });
        },
        getUsers({dispatch, commit}, doc) {
            return new Promise((resolve, reject) => {
                dispatch('get', {
                    name: 'users/all', data: doc
                }).then(
                    json => {
                        commit('setUsers', json)
                        resolve();
                    },
                    err => reject(err)
                );
            });
        },
        setConfirm({dispatch}, doc) {
            return new Promise((resolve, reject) => {
                dispatch('post', {
                    name: 'document/confirm', data: doc
                }).then(
                    () => {
                        dispatch('getList')
                        resolve();
                    },
                    err => reject(err)
                );
            });
        }
    },
    getters: {
        documents (state) {
            return state.documents
        },
        users (state) {
            return state.users
        }
    }
}

export default {
    state: document.state(),
    mutations: document.mutations,
    actions: document.actions,
    getters: document.getters
}

