
const provider = {
    state: () => ({
        providers: [],
    }),
    mutations: {
        setProviders (state, providers) {
            state.providers = providers
        },
    },
    actions: {
        getListProviders({commit, dispatch}, data) {
            return new Promise((resolve, reject) => {
                dispatch('get', {
                    name: 'provider/all', data: data
                }).then(
                    json => {
                        commit('setProviders', json)
                        resolve();
                    },
                    err => reject(err)
                );
            });
        },
        addNewProvider({dispatch}, doc) {
            return new Promise((resolve, reject) => {
                dispatch('post', {
                    name: 'provider/add', data: doc
                }).then(
                    () => {
                        dispatch('getListProviders')
                        resolve();
                    },
                    err => reject(err)
                );
            });
        },
        editProvider({dispatch}, doc) {
            return new Promise((resolve, reject) => {
                dispatch('post', {
                    name: 'provider/edit', data: doc
                }).then(
                    () => {
                        dispatch('getListProviders')
                        resolve();
                    },
                    err => reject(err)
                );
            });
        },
    },
    getters: {
        providers (state) {
            return state.providers
        },
    }
}

export default {
    state: provider.state(),
    mutations: provider.mutations,
    actions: provider.actions,
    getters: provider.getters
}

