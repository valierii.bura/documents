
const product = {
    state: () => ({
        products: [],
    }),
    mutations: {
        setProducts (state, products) {
            state.products = products
        },
    },
    actions: {
        getListProducts({commit, dispatch}, data) {
            return new Promise((resolve, reject) => {
                dispatch('get', {
                    name: 'product/all', data: data
                }).then(
                    json => {
                        commit('setProducts', json)
                        resolve();
                    },
                    err => reject(err)
                );
            });
        },
        addNewProduct({dispatch}, doc) {
            return new Promise((resolve, reject) => {
                dispatch('post', {
                    name: 'product/add', data: doc
                }).then(
                    () => {
                        dispatch('getListProducts')
                        resolve();
                    },
                    err => reject(err)
                );
            });
        },
        editProduct({dispatch}, doc) {
            return new Promise((resolve, reject) => {
                dispatch('post', {
                    name: 'product/edit', data: doc
                }).then(
                    () => {
                        dispatch('getListProducts')
                        resolve();
                    },
                    err => reject(err)
                );
            });
        },
    },
    getters: {
        products (state) {
            return state.products
        },
    }
}

export default {
    state: product.state(),
    mutations: product.mutations,
    actions: product.actions,
    getters: product.getters
}

