import Vue from 'vue'
import Vuex from 'vuex'
import cookie from 'js-cookie';
import router from '../router';
import docModule from './document'
import providerModule from './provider'
import productModule from './products'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        user: {
            session: '',
            access: []
        }
    },
    getters: {
        user: state => {
            return state.user
        }
    },
    mutations: {
        setUser(state, user) {
            state.user = user
        },
        logout(state) {
            state.user = {}
            cookie.set('document-user', '')
        }
    },
    actions: {
        post(_, req) {
            return new Promise((resolve, reject) => {
                window.fetch('http://127.0.0.1:8012/' + req.name, {
                    method: 'POST',
                    credentials: 'include',
                    body: JSON.stringify(req.data)
                })
                    .then(
                        response => {
                            if (!response.ok) {
                                response.json().then(json => {
                                    reject(json);
                                });
                            } else response.json().then(json => resolve(json));
                        },
                        error => console.log(error)
                    );
            });
        },
        put(_, req) {
            return new Promise((resolve, reject) => {
                window.fetch('http://127.0.0.1:8012/' + req.name, {
                    method: 'PUT',
                    credentials: 'include',
                    body: JSON.stringify(req.data)
                })
                    .then(
                        response => {
                            if (!response.ok) {
                                response.json().then(json => {
                                    reject(json);
                                });
                            } else response.json().then(json => resolve(json));
                        },
                        error => console.log(error)
                    );
            });
        },
        get(_, req) {
            return new Promise((resolve, reject) => {
                window.fetch('http://127.0.0.1:8012/' + req.name, {method: 'GET', credentials: 'include'})
                    .then(
                        response => {
                            if (!response.ok) {
                                response.json().then(json => {
                                    reject(json);
                                });
                            } else response.json().then(json => resolve(json));
                        },
                        error => console.log(error)
                    );
            });
        },
        login({commit, dispatch}, data) {
            return new Promise((resolve, reject) => {
                dispatch('post', {
                    name: 'auth/login', data: data
                }).then(
                    json => {
                        commit('setUser', json);
                        router.push('/')
                        resolve();
                    },
                    err => reject(err)
                );
            });
        },
        getSession({commit, dispatch}) {
            var s = cookie.get('document-user');
            return new Promise((resolve, reject) => {
                dispatch('post', {name: 'auth/checksession', data: {session: s}}).then(
                    json => {
                        commit('setUser', json);
                        resolve();
                    },
                    err => {
                        router.push('/login');
                        console.log('redirect')
                        reject(err);
                    }
                );
            });
        }
    },
    modules: {
        document: docModule,
        provider: providerModule,
        product: productModule,
    }
})
